\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{listings}
\usepackage{multicol}
\usepackage[backend=bibtex]{biblatex}
\addbibresource{bibliography.bib}
\usetheme{CambridgeUS}

\graphicspath{ {images/} }

\title{\b Crowdsourcing scoring of immunohistochemistry
images: Evaluating Performance of the Crowd and
an Automated Computational Method}

\author{Humayun Irshad, Eun-Yeong Oh, Daniel Schmolze, Liza M. Quintana,\\ Laura
Collins , Rulla M. Tamimi and Andrew H. Beck}

\date{21 June 2016}

\begin{document}
\frame{\titlepage}
\begin{frame}
\frametitle{Contents}
\begin{multicols}{2}
\tableofcontents
\end{multicols}
\end{frame}
\begin{frame}
\begin{abstract}
The assessment of protein expression in immunohistochemistry (IHC) images provides important diagnostic, prognostic and
predictive information for guiding cancer diagnosis and therapy. Manual scoring of IHC images represents a logistical challenge,
as the process is labor intensive and time consuming. Since the last decade, computational methods have been developed to
enable the application of quantitative methods for the analysis and interpretation of protein expression in IHC images. These
methods have not yet replaced manual scoring for the assessment of IHC in the majority of diagnostic laboratories and in many
large-scale research studies. An alternative approach is crowdsourcing the quantification of IHC images to an undefined crowd.
The aim of this study is to quantify IHC images for labeling of ER status with two different crowdsourcing approaches, image
labeling and nuclei labeling, and compare their performance with automated methods. 
\end{abstract}
\end{frame}
\section{Introduction}
\begin{frame}
\frametitle{Introduction}
Immunohistochemistry (IHC) is widely used for measuring the presence and location of protein expression in tissues. The
assessment of protein expression by IHC provides important diagnostic, prognostic and predictive information for guiding
cancer diagnosis and therapy. In the research setting, IHC is frequently evaluated using tissue microarray (TMA) technology, in
which small cores of tissue from hundreds of patients are arrayed on a glass slide, enabling the efficient evaluation of biomarker
expression across large numbers of patients.\par
The manual pathological scoring of large numbers of TMAs represents a logistical challenge, as the process is labor
intensive and time consuming. Over the past decade, computational methods have been developed to enable the application of
quantitative methods for the analysis and interpretation of IHC-stained histopathological images\alert{\cite{1,2}}. While some automated
methods have shown high levels of accuracy for IHC markers, automated analysis has not yet replaced manual scoring for
the assessment of IHC in the majority of diagnostic pathology laboratories and in many 
\end{frame}
\begin{frame}
large-scale research studies.\par
In this study, we evaluate the use of crowdsourcing to outsource the task of scoring IHC labeled TMAs to a large crowd
of users not previously trained in pathology. Over the last decade, crowdsourcing has been used in a wide range of domains,
including astronomy\alert{\cite{3}}, zoology, medical microbiology, and neuroscience, to achieve tasks that required large-scale
human labeling, which would be difficult or impossible to achieve effectively using only computational methods or domain
experts.\par
In a pilot study, we explored the use of crowdsourcing for rapidly obtaining annotations for two core tasks in computational
pathology: nucleus detection and segmentation\alert{\cite{4}}. This study concluded that aggregating multiple annotations from a crowd to
obtain a consensus annotation could be used effectively to generate large-scale human annotated datasets for nuclei detection
and segmentation in histopathological images. Crowdsourcing has also recently been evaluated for immunohistochemistry
studies. Mea et al. crowdsourced 13 IHC images for detection of positive and negative nuclei and reported 0.95 Spearman
correlation between pathologist and crowdsourced positivity percentages\alert{\cite{5}}.  
\end{frame}
\begin{frame}
Recently, the Cell Slider project CellSlider by
Cancer Research UK provided an online interface for members of the general public to score IHC stained TMA images, and they
arXiv:1606.06681v1  [cs.CV]  21 Jun 2016
reported high levels of concordance of crowdsourced scores obtained from non-experts and the scores of trained pathologists.\par
The purpose of the present study is two-fold. First, we aim to evaluate the performance of crowdsourcing vs. an automated
method for scoring protein expression in IHC stained TMA images. Second, we aim to evaluate the time, cost, and accuracy of
two different approaches to crowdsourcing the IHC task (image-level labels vs. nucleus-level labels).
\end{frame}
\section{Methods}
\subsection{Dataset}
\begin{frame}
\frametitle{Methods}
This study used IHC-stained TMA images of breast cancer tissue from the Nurses’ Health Study (NHS). The dataset consists of 5,483 scanned images of TMA cores, which were immunostained for estrogen receptor (ER) and scanned using Aperio Slide Scanner at 20x magnification. The average image size is 828 x 848 pixels. These images are derived from 1909 patients, each of whom contributed 1 - 3 TMA images, with more than half of the patients contributing TMA images. All study images were scored by an expert breast pathologist, using three labels (negative=0, low positive=1 and positive=2).
\end{frame}
\subsection{Crowdsourcing Platform}
\begin{frame}
We employed the CrowdFlower platform to design both crowdsourcing applications (image labeling and nuclei labeling).
CrowdFlower is a crowdsourcing platform that works with over 50 labor channel partners to enable access to a network of
more than 5 million contributors worldwide. This platform offers a number of features to improve the likelihood of obtaining
high-quality work from contributors. In CrowdFlower, the job designer creates a job in the form of tasks, which are served to
contributors for labeling. Each task is a collection of one or more images sampled from the data set. The job designer creates
test questions (test images which have been previously labeled by pathologists) that are used for dual purposes: qualification of
contributors during quiz mode and monitoring of contributors during judgment mode. Contributors must maintain a defined
level of accuracy on the test questions to be permitted to complete the job. In addition, the job designer specifies the payment
per task and the number of labels desired per image. After job completion, CrowdFlower provides a list of labels (annotations)
for all the images. Additional information on the CrowdFlower platform is available at www.crowdflower.com.
\end{frame}
\subsection{Job Design and Crowdsourcing Applications}
\begin{frame}
Each crowdsourcing job has two modes: quiz mode and judgment mode. Quiz mode occurs at the beginning of a job. In quiz
mode, there is only one task and the task consists of 5 test question images. In judgment mode, there are a number of tasks
and each task consists of 4 actual images and one test image which is presented to the contributor in the same manner as the
unlabeled images such that the contributor is unaware if he/she is annotating an unlabeled image or test image. Each contributor
must qualify during quiz mode to enter in judgment mode and can remain in judgment mode as long as his/her accuracy on
test questions is above a threshold level. For ensuring high quality of labels, we defined five parameters which may influence
labeling performance.
\end{frame}
\begin{frame}
\begin{itemize}
\item The first is test question minimum accuracy that ensures each contributor must maintain minimum 60\% accuracy on test questions throughout the job completion.
\item The second is minimum time per task that ensures each contributor must spend a minimum of 10 seconds to complete
one task.
\item The third is maximum number of judgments per contributor that enable more contributors to participate in the job. In our
jobs, we defined maximum number of judgment per contributor 500 judgments.
\item The fourth is a minimum number of images (20) for the contributor to review in work mode prior to computing a trust
score for each worker and prior to filtering workers based on their trust score.
\item The fifth is the number of labels to collect per image. We collect three labels per image for both jobs.
\end{itemize}
\end{frame}
\begin{frame}
Our study includes two types of labeling jobs: image labeling and nuclei labeling. \alert{Figure \ref{fig:Figure1}} illustrates the flow chart of both
crowdsourcing jobs. Each job contains instructions, which provide examples of expert-derived labels and guidance to assist the
contributor in learning the process of labeling.
\end{frame}
\subsubsection{Image Labeling}
\begin{frame}
In the image labeling job, each contributor estimates the percentage of cancer nuclei stained brown (positive) and blue (negative)
in the image and then selects the image label (score) depending on given criteria: if percentage of brown nuclei is less than
1\% then image label is A (negative protein expression), if percentage is between 1\% and 10\% , then image label is B (low positive protein expression), if percentage is between 10\% and 50\% then image label is C (positive protein expression) and if percentage is more than 50\% then image label is D (high positive protein expression). The total pool of test question images used in both
quiz and judgment modes are 250, which are labeled by pathologists.\alert{Figure \ref{fig:Figure2}} shows the interface for image labeling.
\end{frame}
\begin{frame}
\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{Figure1}
\caption{Crowdsourcing work flow for Image Labeling and Nuclei Labeling.}
\label{fig:Figure1}
\end{figure}
\end{frame}
\subsubsection{Nuclei Labeling}
\begin{frame}
In the nuclei labeling job, we ask contributors to detect positive and negative nuclei in the image. In the nuclei labeling job, we
first ask contributors to identify the presence of nuclei in the image (yes/no). If they do identify the presence of nuclei, then we
ask the contributor to label the nuclei using a dot operator (by clicking at the center of each nucleus). At completion of job,
CrowdFlower provides the position of positive and negative nuclei in the images. For each image, we collected positive and
negative nuclei from three different contributors. The total pool of test question images used in both quiz and judgment modes
for nuclei labeling are 100 images, which are labeled by pathologists. After counting number of positive and negative nuclei,
we compute the positivity index ( $PIndex = \frac{No. of Positive Nuclei}{Total No.of Nuclei}$. From positivity index, we compute the image label using image
labeling criteria (mentioned in Image Labeling section).\alert{Figure \ref{fig:Figure3}} shows the interface for nuclei labeling.
\end{frame}
\subsection{Aggregation Methods for Image Labeling Problem}
\begin{frame}
We calculated the aggregated label for each image using four different methods: maximum crowd votes (CV), maximum crowd
trust scores (CT), maximum weighted crowd votes ($\omega$CV) and maximum weighted crowd trust scores ($\omega$CT). CV is computed by summing the votes for each label and selecting the label with the maximum number of votes as the aggregated label. CT is computed by summing the contributor trust score (CT) for each label and selecting the label with the maximum trust score as the aggregated label. For $\omega$CV and $\omega$CT methods, we multiply the class weights with crowd votes for each label and crowd trust scores for each label, respectively.
\end{frame}
\begin{frame}
\begin{alertblock}{Equation}
\begin{equation}
WeightedCrowdVote = \frac{\omega_AV_A + \omega_BV_B + \omega_CV_C + \omega_DV_D}{V_A + V_B + V_C + V_D}
\end{equation}
\end{alertblock}
\begin{alertblock}{Equation}
\begin{equation}
WeightedCrowdTrustScore = \frac{\omega_AT_A + \omega_BT_B + \omega_CT_C + \omega_DT_D}{T_A + T_B + T_C + T_D}
\end{equation}
\end{alertblock}
\end{frame}
\begin{frame}
where $V_A$, $V_B$, $V_C$, and $V_D$ are crowd from each class labels; $T_A$, $T_B$, $T_C$ and $T_D$ are sum of crowd trust scores for each
class labels; and $\omega_A$, $\omega_B$, $\omega_C$ and $\omega_D$ are class weights. We calculated the class weights by taking the mean of lower and upper boundary of the class. For class A, lower boundary is 0 and upper boundary is 0.01, the weight of class is 0.005. For class B, lower boundary is 0.01 and upper boundary is 0.1, the weight of class B is 0.05. For class C, lower boundary is 0.1 and upper boundary is 0.5, the weight of class C is 0.3. For class D, lower boundary is 0.5 and upper boundary is 1, the weight of class D is 0.75. The aggregated label selected is the label whose class bounds contain the weighted crowd vote or weighted crowd trust score.
\end{frame}
\begin{frame}
\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{Figure2}
\caption{Crowdsourcing Application interface for image labeling. The screenshot illustrates the interface for selecting the
image class label.}
\label{fig:Figure2}
\end{figure}
\end{frame}
\subsection{Sensitivity Analysis for Different Combinations of Crowd Size}
\begin{frame}
To estimate the number of crowd labels required to generate optimal aggregated crowd label, we performed a sensitivity analysis
of aggregated labels using different combination of crowd sizes. For this pilot study, we collected 10 crowd labels for each
image, and we computed the aggregated label of each image using different combination of crowd sizes (1 to 10), according to
\alert{Algorithm \ref{alg1}}.
\end{frame}
\subsection{Performance Measures}
\begin{frame}
We explored different performance measures to evaluate inter-observer reliability of scores. For measuring the inter-observer
reliability, we measured percent agreement $(A_g)$ or accuracy, which is calculated as the number of agreed labels divided by
total number of labels, Kappa $(\kappa)$ which measures the agreement among observers adjusted for the possibility of by chance
agreement, Spearman correlation $(\rho)$ hich measures the mean of bivariate Spearman’s rank correlations between observers for
inter-observer reliability, and intra-class correlation (ICC). For image classification, we used $A_g$ performance measures for
comparing different methods of label aggregation and the automated method.
\end{frame}
\begin{frame}
\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{Figure3}
\caption{Crowdsourcing application interface for nuclei labeling. The screenshot illustrates the interface for labeling the
positive and negative nuclei separately.}
\label{fig:Figure3}
\end{figure}
\end{frame}
\section{Results}
\subsection{Image labeling on 380 TMA cores - A pilot study}
\begin{frame}
\frametitle{Results}
We designed a pilot study to test the crowd sourcing application for IHC image labeling and to assess the improvement in
crowdsourcing performance as we increase the numbers of aggregated instances per image. In the pilot study, we collected 10
crowdsourced labels for 380 images. We also collected three pathologist labels for each of these 380 images using the same
crowd sourcing interface.\par
We assessed inter-observer reliability among pathologists using 4-class labeling as well as 2-class labeling as shown in
\alert{Figure 4a(pg.\ref{fig:Figure4a})}. For 2-class labeling, we merged all positive classes (B, C and D) into a single positive class (B). We observed
Kappa values of 0.43 and 0.5 for 2-class and 4-class  labeling, respectively, indicating moderate inter-pathologist agreement in
IHC interpretation.\par
For 380 images, we obtained 10 crowd labels per image and compared these scores with the pathologist scores as shown in
\alert{Figure 4b(pg.\ref{fig:Figure4b})}. We found a wide range of agreement on images between the crowd and the pathologist scores, with a median level of agreement of 6/10 and 15\% of images 
\end{frame}
\begin{frame}
showing 10/10 agreement. For a range of crowd labels per image (ranging from 1 to
10), we computed aggregated labels and assessed the agreement of the consensus score with the pathologist score for each
number of crowd labels as reported in \alert{Figure 6a(pg.\ref{fig:Figure6a})} and \alert{6b(pg.\ref{fig:Figure6c})}. The $A_g"$ is not significantly improved after crowd size 3 for 4-class
and 2-class image labeling problem.
\end{frame}
\begin{frame}
\begin{algorithm}[H]
\caption{Sensitivity Analysis for Different Crowd Sizes}\label{alg1}
\begin{algorithmic}[1]
\ForAll{Crowd Sizes: $C_i$ in $i$ $\in$ 1,2,3,...,10}
\State $P$ = Compute combination patterns (without replacement) for Crowd Size $C_i$
	\ForAll{Pattern: $P_j$ in $j$ $\in$ 1,2,3,...,$J$}
		\ForAll{Images: $I_k$ in $k$ $\in$ 1,2,3,...,$K$}
			\State Compute the Aggregated labels for combination pattern $P_j$ of Crowd Size $C_i$ for Image $I_k$
		\EndFor
	\EndFor
\EndFor
\end{algorithmic}
\end{algorithm}
\end{frame}
\begin{frame}
\begin{figure}[h]
\includegraphics[width=0.4\textwidth]{Figure4a}\label{fig:Figure4a}
\includegraphics[width=0.4\textwidth]{Figure4b}\label{fig:Figure4b}
\caption{Inter-observer reliability of pathologist labels and agreement with crowd labels}
\label{Figure 4}
\end{figure}
\end{frame}
\begin{frame}
\begin{figure}[h]
\includegraphics[width=0.4\textwidth]{Figure5a}\label{fig:Figure5a}
\includegraphics[width=0.4\textwidth]{Figure5b}\label{fig:Figure5b}
\caption{Sensitivity analysis of crowd labels in pilot study. The analysis supports using 3 crowdsourced labels per image.}
\label{Figure 5}
\end{figure}
\end{frame}
\subsection{Image labeling on 5483 TMA cores}
\begin{frame}
Based on the results of the sensitivity analysis, we collected 3 crowd labels for each image for the image and nucleus labeling
crowdsourcing jobs for the main study. The image and nuclei labeling work flow is illustrated in \alert{Figure \ref{fig:Figure1}}. For 4-class image
labeling, we collected 3 labels for each TMA image using CrowdFlower as shown in \alert{Figure \ref{fig:Figure2}}. In total, 16,449 image labels
were collected for 5,483 images. Aggregated image labels were computed with four aggregation methods (CV,CT,$\omega$CV and $\omega$CT).The aggregated label at patient level was computed by taking the median of all aggregated image labels belonging to
that patient. Pathologists labeled these images using 3-class labeling: negative (A), low positive (B) and positive (C). Since we
obtained 4-class labeling from the crowd, to compare the crowd labels with pathologist labels, we merged crowd class D into
crowd class C. The CV aggregation method reported higher $A_g$ and Spearman $\rho$ than other aggregation methods for 3-class
labeling as reported in \alert{Table \ref{table:1}}. For 2-class labeling, we merged all positive classes (B and C) into a single positive class (B) for
both crowd and pathologist aggregated labels. The CV aggregation method outperformed as compared to other aggregation
method for $A_g$ and $\rho$ as reported in \alert{Table \ref{table:1}}.
\end{frame}
\begin{frame}
\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
\multirow{2}{*}{Crowdsourcing Types} & \multirow{2}{*}{Methods} & \multicolumn{2}{c|}{3-Class Labeling} & \multicolumn{2}{c|}{2-Class Labeling} \\ \cline{3-6} 
                                     &                          & $A_g$             & $\rho$            & $A_g$             & $\rho$            \\ \hline
\multirow{4}{*}{Image Labeling}      & Crowd CV                 & \textbf{0.71}     & \textbf{0.64}     & \textbf{0.83}     & \textbf{0.62}     \\ \cline{2-6} 
                                     & Crowd CT                 & 0.68              & 0.65              & 0.81              & 0.61              \\ \cline{2-6} 
                                     & Crowd $\omega$CV         & 0.64              & 0.63              & 0.77              & 0.59              \\ \cline{2-6} 
                                     & Crowd $\omega$CT         & 0.64              & 0.64              & 0.77              & 0.59              \\ \hline
\multirow{2}{*}{Nuclei Labeling}     & Crowd                    & \textbf{0.77}     & \textbf{0.68}     & \textbf{0.87}     & \textbf{0.63}     \\ \cline{2-6} 
                                     & Definiens                & 0.70              & 0.51              & 0.81              & 0.48              \\ \hline
\end{tabular}
\caption{Crowdsourced image labeling and nuclei labeling results}
\label{table:1}
\end{table}
\end{frame}
\subsection{Crowdsourcing Performance}
\begin{frame}
We first assessed the contributor (crowd) performance for both crowdsourcing jobs. The number of contributors who participated
in both jobs is shown in \alert{Table \ref{table:2}}.  The contributors who maintained the minimum accuracy
(60\%) on test questions during
quiz and work modes are trusted contributors and the rest are untrusted contributors.  In work mode, there were 61 trusted
contributors for image labeling and 2,216 for nuclei labeling.  The average time of trusted contributors was 32 seconds for
image labeling and 306 seconds for nuclei labeling per image while the average time of untrusted contributors was 149 seconds
for image labeling and 207 seconds for nuclei labeling. Thus, trusted contributors took less time to label images as compared
to untrusted contributors; however, trusted contributors took more time to label nuclei as compared to untrusted contributors.
These results suggest that efficient labeling of nuclei is a complex job requiring sufficient time for strong performance. \alert{Figure 6(pg.\ref{fig:Figure6a})} illustrates the distribution of crowd trust scores for both jobs.  The image labeling contributors have higher trust score as
compared to nuclei labeling contributors. The average test question accuracy for trusted contributors is
80\% for image labeling and 76\% for nuclei labeling 
\end{frame}
\begin{frame}
while average test question accuracy for untrusted contributors is 66\% for image labeling and 42\% for nuclei labeling. The trust scores were moderately correlated with the number of images labeled; $\rho = 0.41, P < 0.0008$ for image labeling job and $\rho = 0.186, P < 2.2e^-16$ for nuclei labeling. The average time for image labeling is 50 seconds and nuclei labeling per image is 373 seconds.\par 
The image labeling job was finished in 4 hours and nuclei labeling was finished in 472 hours. The Crowdflower platform
charged \$ 282 for image labeling and \$ 2,280 for nuclei labeling job. These data suggest that although nuclei labeling produced
some improvements in accuracy, it cost significantly longer in terms of time to complete the full job (118 fold longer) and cost
( 8 fold more expensive).
\end{frame}
\begin{frame}
\begin{table}[h!]
\begin{tabular}{|c|c|c|c|c|}
\hline
\multirow{2}{*}{Crowdsourcing Jobs} & \multicolumn{2}{c|}{Quiz Mode} & \multicolumn{2}{c|}{Work Mode} \\ \cline{2-5} 
                                    & Passed         & Failed        & Passed         & Failed        \\ \hline
Image Labeling                      & 113            & 155           & 61             & 52            \\ \hline
Nuclei Labeling                     & 3244           & 1572          & 2216           & 1243          \\ \hline
\end{tabular}
\caption{Crowd performance on test questions in quiz mode and work mode.}
\label{table:2}
\end{table}
\end{frame}
\begin{frame}
\begin{figure}[h]
\includegraphics[width=0.3\textwidth]{Figure6a}\label{fig:Figure6a}
\includegraphics[width=0.3\textwidth]{Figure6b}\label{fig:Figure6b}
\includegraphics[width=0.3\textwidth]{Figure6c}\label{fig:Figure6c}
\caption{Contributor trust scores analysis on Crowdsourcing jobs.}
\label{Figure 6}
\end{figure}
\end{frame}
\section{Discussion}
\begin{frame}
\frametitle{Discussion}
The principle of applying crowdsourcing in science, which has enabled whale sound classification, malaria parasite classification, sleep spindle detection and nuclei detection and segmentation in histopathology, has become increasingly well
established in recent years. Crowdsourced work can be used to classify objects (whale sound and malaria parasite classification),
detect objects (nuclei detection) and segment objects (nuclei segmentation). The aim of this study was to better understand how
to use crowdsourcing for IHC image interpretation. This laborious and time consuming image quantification task has also been
performed using automated methods. However, no prior studies have directly compared crowdsourcing vs. automated
methods in the interpretation of IHC.
In this study, we quantify IHC TMA images for labeling of ER status with two different crowdsourcing approaches, image
labeling and nuclei labeling. In the image labeling task, the crowd was asked to estimate the percentage of positive cells for each
IHC image, while in nuclei labeling task, the crowd was asked to label individual nuclei within IHC images as either 
\end{frame}
\begin{frame}
positive or
negative. We completed these crowdsourcing tasks on a large data set containing 5483 TMA images belonging to 1909 patients,
which were previously labeled by an expert pathologist and by an automated method. In our study, crowdsourcing-derived
scores obtained greater concordance with the pathologist interpretation for both image labeling and nuclei labeling tasks, as
compared with the pathologist concordance achieved by the automated method.
Overall, the crowdsourced scores produced from nuclei labeling (as opposed to image labeling) showed somewhat higher
agreement with the pathologist scores; however, the time and cost required for the nuclei labeling far exceeded the time and cost
for the image labeling. Nuclei labeling is more laborious task spread over many people, even though paying more and takes
longer time for nuclei scoring, still cheaper and quicker than using pathologists. Our study results support that crowdsourcing is
a promising new approach for scoring biomarker studies in large scale cancer molecular pathology studies. A limitation of our
current crowdsourcing application is that we do not ask the Crowd to classify nuclei into specific types (e.g., cancer epithelial
nucleus, lymphocyte nucleus). We expect the addition of training the 
\end{frame}
\begin{frame}
crowd to classify cell types in addition to classifying IHC
positivity will further improve crowd performance, although the incorporation of cell type-specific scoring may increase the
time and cost of the overall task. This represents an important direction for future research.
\end{frame}
\section{Bonus information}
\subsection{Divide et impera}
\begin{frame}
\frametitle{Bonus Information}
\lstinputlisting[language=C]{Divideetimpera.c}
\end{frame}
\subsection{Criticality Aware Multiprocessor}
\begin{frame}
Criticality  aware  multiprocessor  provides  a  new  direction  for  tapping  performance  in a shared  memory  multiprocessor  and  can  provide  substantial  speedup  in  lock  intensive benchmarks. To  tap  the  full  potential  of  criticality  aware  multiprocessor,  there  are  many  avenues which needs further investigation.
\begin{enumerate}
\item Benchmarks:  It  would  be  interesting  to  see  how  CAM  performs  in  a  real benchmark suite which has lot of locks. 
\item Switches:  A  more  realistic  modeling  of  switch  could  result  in  more  contention between  critical  and  non-critical  requests. This  could  result  in  CAM  performing even better.
\item Barriers:  Identifying  other  sources  of  criticality  like  barriers  can  help  increasing the  performance of criticality aware multiprocessor
\item Out  of  order  processor  model  and  CMPs: Present  investigation  used  a  simple inorder model and SMP. It would be interesting to see how CAM performs using an out of order processor model/CMP.
\end{enumerate}
\end{frame}
\subsection{Neural Network Framework}
\begin{frame}
As we discuss in our earlier paper (Hejazi et al., 2015), spatial interpola-
tion techniques can provide efficient and accurate estimation of the Greeks for
a large portfolio of VA products.  Although IDW and RBF methods provide
better efficiency and resolution than Kriging methods, they are less accurate
than Kriging methods.  Our experiments in (Hejazi et al., 2015) demonstrate
the signi cance of the choice of distance function on the accuracy of IDW
and RBF methods.  A manual approach to  nd the best distance function
that minimizes the estimation error of the IDW and the RBF methods for
a given set of input data is not straightforward and requires investing a sig-
ni cant amount of time.  The difficulty in  nding a good distance function
diminishes the e ectiveness of the IDW and the RBF methods.
In order to automate our search for an e ective distance function while
maintaining the efficiency of the IDW and the RBF methods, we propose a
machine learning approach.  In our proposed approach, we use an extended
5 version  of  the  Nadaraya-Watson  kernel  regression  model  (Nadaraya,  1964;
Watson,  1964)  to  estimate  the  Greeks.   Assuming $y(z_1),...,y(z_n)$ are  the
observed values at known locations $z_1, ... , z_n$, the Nadaraya-Watson estimator approximates the value $y(z)$ at
\end{frame}
\begin{frame}
 the location $z$ by where $K_h$ is a kernel with a bandwidth of $h$. The Nadaraya-Watson estimator
was  rst proposed for kernel regression applications and hence the choice of
kernel function $K_h$ was a necessity. For our application of interest, we choose
to use the following extended version of the Nadaraya-Watson estimator:
\begin{alertblock}{Equation}
\begin{equation}
y(z) = \sum_{i=1}^{n} \frac{K_h(z-z_i) + y(z_i)}{\sum_{j=1}^{n} K_h(z-z_j)}
\end{equation}
\end{alertblock}
\begin{alertblock}{Equation}
\begin{equation}
\label{eq4}
y(z) = \sum_{i=1}^{n} \frac{G_{h_j}(z-z_i) + y(z_i)}{\sum_{j=1}^{n} G_{h_i}(z-z_j)}
\end{equation}
\end{alertblock}
\end{frame}
\begin{frame}
where $G$ is a nonlinear di erentiable function and the subscript $h_i$, similar to th bandwidth $h$ of kernels, denotes the range of influence of each $y(z_i)$ on the estimated value. Unlike the Nadaraya-Watson model, the $h_i$s are not universal free parameters and are location dependent. Moreover, $h_i$ is a vector that determines the range of influence of each pointwise estimator in each direction of feature space of the input data. As we discuss below, our decision to calibrate the $h_i$ parameters using a neural network necessitated the properties of $G$.\par
In our application of interest, the $z_i,1 \leq i \leq n$, in \alert{Equation \ref{eq4}} define a set of VA contracts, called representative contract, and $y(z_i), 1 \leq i \leq n$, are their corresponding Greek values. Hence, \alert{Equation \ref{eq4}} is similar to the equation for the IDW estimator. The $G_{h_i}(z-z_i)$ in \alert{Equation \ref{eq4}} is comparable to the weight (inverse of the distance for representative contract $z_i$ in the equation of IDW. Therefore, once we know the particular choices of the $h_i$s for the $G$ function for each of our $n$ representative VA contracts, we can compute the Greeks for a large portfolio of $N$ VA contracts in time proportional to $N$ x $n$, wich preserves the efficiency of our framework. In order to find a good choice of the $G_{h_i}$ 
\end{frame}
\begin{frame}
functions, we propose the use of a particular type of neural network called a feed-forward neural network. As we describe in more detail below, our choice of neural network allows us to find an effective choice of the $G_{h_i}$ functions by finding the optimum choices of the $h_i$ values that minimize our estimation error, and hence eliminate the need for a manual search of a good choice of distance function.\par
Feed-forward  networks  are  well-known  for  their  general  approximation
properties which has given them the name of universal approximators.  For
example a one-layer feed-forward network with linear outputs is capable of
approximating any continuous function on a compact domain (Hornik, 1991).
For a thorough study of feed-forward networks, the interested reader is re-
ferred to (Bishop, 2006) and the references therein.  For the sake of brevity,
in the rest of this paper, we use the word neural network to refer to this par-
ticular class of feed-forward neural network unless explicitly said otherwise.
\end{frame}
\section{References}
\begin{frame}[allowframebreaks]
\frametitle{References}
\nocite{*}
\printbibliography
\end{frame}


\end{document}