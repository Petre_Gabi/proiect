int SumaDi(int v[], int p, int u){
	if (p == u) return v[p];
	else
	{
		int m = (p+u)/2;
		int S1 = SumaDi(v,p,m);
		int S2 = SumaDi(v,m+1,u);
		return S1+S2;
	}
}
